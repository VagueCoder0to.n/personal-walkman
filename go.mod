module gitlab.com/VagueCoder0to.n/Personal-Walkman

go 1.16

require (
	github.com/cassava/lackey v0.5.1 // indirect
	github.com/mewkiz/flac v1.0.7
	github.com/stretchr/testify v1.7.0
)
