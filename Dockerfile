FROM golang:1.15.6-alpine3.12

# Setting environment variables
ENV PROJECT_ROOT=$GOPATH/src/gitlab.com/VagueCoder0to.n/Personal-Walkman
ENV ENV GOBIN $GOPATH/bin
ENV PATH=$PATH:$GOBIN

# Installing packages
RUN apk add --no-cache \
    # GCC Compiler and related packages. General.
    gcc build-base musl-dev \
    # Package for formatting audio files. Application specific.
    ffmpeg \
    ## General prerequisites
    # make git \
    ## Libtool helps in package building in GCC.
    # libtool \
    ## To work with certs
    # ca-certificates \
    ## dumb-init: Runs as PID 1; Acts as init system; Useful in docker containers; Launches single process and then proxies all received signals to child process (which is our application); Just needs to be prepended with any command like "dumb-init bash echo Hi";
    dumb-init 

# Setting Directory
RUN mkdir -p $PROJECT_ROOT
WORKDIR $PROJECT_ROOT

# Build Application
COPY . .
RUN go mod download
RUN go build -o=$GOBIN/walkman $PROJECT_ROOT/app;

# Set runnable command
ENTRYPOINT ["/usr/bin/dumb-init", "--"]
CMD walkman
