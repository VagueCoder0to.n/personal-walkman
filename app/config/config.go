package config

import "os"

var (
	PROJECT     string
	SONGS_HOME  string
	PY_ACTIVATE string
)

func init() {
	PROJECT = os.Getenv("PROJECT")
	SONGS_HOME = os.Getenv("SONGS_HOME")
	PY_ACTIVATE = os.Getenv("PY_ACTIVATE")
}
