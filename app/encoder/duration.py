#!/usr/bin/env python3
import sys
from tinytag import TinyTag

if __name__ == "__main__":
    tag = TinyTag.get(sys.argv[1])
    print(round(tag.duration))
