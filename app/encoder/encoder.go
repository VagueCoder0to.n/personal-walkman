package encoder

import (
	"fmt"
	"log"
	"os/exec"

	"gitlab.com/VagueCoder0to.n/Personal-Walkman/app/config"
)

func Encode(filename string) error {
	// ext := path.Ext(filename)
	places := 3
	songDir := fmt.Sprintf("%s/%s/", config.SONGS_HOME, filename)
	audioFile := fmt.Sprintf("%s%s.mp3", songDir, filename)
	outputMetaFile := fmt.Sprintf("%s%s_outputlist.m3u8", songDir, filename)
	outputFileFormat := fmt.Sprintf("%s%s_output%%0%dd.ts", songDir, filename, places)
	length := 10

	// cmd := exec.Command("ffmpeg",
	// 	"-i", audioFile, // Input file
	// 	"-c:a", "libmp3lame", // Codec of Audio
	// 	"-b:a", "320k", // Bitrate of Audio
	// 	"-map", "0:0", // Maps input to output. 0:0 means mapping first i/p to first o/p.
	// 	"-f", "segment",
	// 	"-segment_time", fmt.Sprint(length),
	// 	"-segment_list", outputMetaFile,
	// 	"-segment_format", "mpegts", outputFileFormat,
	// )

	cmd := exec.Command("ffmpeg",
		"-i", audioFile, // Input file
		"-c:a", "libmp3lame", // Codec of Audio
		"-b:a", "320k", // Bitrate of Audio
		"-map", "0:0", // Maps input to output. 0:0 means mapping first i/p to first o/p.
		"-f", "segment",
		"-segment_time", fmt.Sprint(length),
		"-segment_list", outputMetaFile,
		"-segment_format", "mpegts", outputFileFormat,
	)

	fmt.Println(cmd)
	return cmd.Run()
}

/*

ffmpeg
-i BachGavotteShort.mp3
-c:a libmp3lame
-b:a 128k
-map 0:0
-f segment
-segment_time 10
-segment_list outputlist.m3u8
-segment_format mpegts
output%03d.ts

*/

// TODO: To source the venv and call the ffmpeg command
func FileSize(filename string) {
	// cmd := exec.Command("bash", "-c", "source "+config.PY_ACTIVATE+"; pip list")
	cmd := exec.Command("bash", "-c", "source "+config.PY_ACTIVATE+";",
		config.PROJECT+"/app/encoder/duration.py", config.SONGS_HOME+"/PhirSeUdChala/PhirSeUdChala.flac;",
		"deactivate;")

	fmt.Println(cmd)

	op, err := cmd.CombinedOutput()
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(string(op))

}
