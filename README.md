# :warning: `This is still under development!!`

# Personal-Walkman
As the name suggests, this is a audio streaming project to structure in Golang. The aim is to stream in lesser bandwidth, and host on a remote server (like) environment. Nothing new, nothing fancy, just a basic approach.

---
## Message to Viewer
A geek to geek: This is an open-source, in-development, no-rush kinda project. However, if you somehow liked the idea and wish to contribute; or liked the code snippet and wish to use for your own; I'd be glad to share.

You can let me know your ideas, or any issues and queries with this project, or if you want to further discuss. Just write to me at (Click): [VagueCoder0to.n@gmail.com](mailto:VagueCoder0to.n@gmail.com?subject=%5BGITLAB%3A%20Personal-Walkman%5D%20Your%20Subject%20Here&body=Hello%20Vague%2C%0A%0A)

## Happy Coding !! :metal:

